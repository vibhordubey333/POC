package handler

import (
	"fmt"
	"net/http"
)

func Hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hola")

}

func Goodbye(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Adios")

}
